# Simple example of Spring Security with JPA #

## Prerequisites ##

> java 8

> maven 3

## Configuration ##

None, it uses H2 to store data.

## Run ##

> mvn spring-boot:run

## Documentation ##

### Enabling security ###

```
@EnableWebSecurity 
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, UserDetailsService userDetailsService) throws Exception {
        auth.userDetailsService(userDetailsService);
    }
}
```

This annation `@EnableGlobalMethodSecurity` allows to annotate methods with `@Secure("ROLE_USER")` to restrict access to people with the role USER.

The `configureGlobal` defines the component responsible for building an instance of `UserDetails` from a username. This is where we retrieve the user with its roles from database.

### Building UserDetails from username ###

```
@Component
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);
        List<GrantedAuthority> authorities = user.getUserRole()
            .stream()
            .map(UserRole::getRole)
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());

        return new org.springframework.security.core.userdetails.User(
            user.getUsername(),
            user.getPassword(),
            true,
            true,
            true,
            true,
            authorities
        );
    }
}
```

In here, we just retrieve a user with its roles from database by using the username specified in the login form. Then we build an instance of UserDetails which is needed by spring security to validate the credentials and creating a session. 
