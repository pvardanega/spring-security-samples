package com.example;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class SecuredController {

    @RequestMapping(value = "admin", method = {GET, POST})
    @Secured("ROLE_ADMIN")
    public String admin() {
        return "admin";
    }

    @RequestMapping(value = "user", method = {GET, POST})
    @Secured("ROLE_USER")
    public String user() {
        return "user";
    }
}
