insert into user (id, username, password, enabled) values (1, 'admin', 'admin', 1), (2, 'user', 'user', 1);
insert into user_roles (user_id, role) values (1, 'ROLE_ADMIN'), (2, 'ROLE_USER');
