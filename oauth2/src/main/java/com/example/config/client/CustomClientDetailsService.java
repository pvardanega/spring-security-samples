package com.example.config.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Component;

@Component
public class CustomClientDetailsService implements ClientDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        Client client = clientRepository.findOne(clientId);
        BaseClientDetails clientDetails = new BaseClientDetails(
            client.getId(),
            null,
            client.getScopes(),
            client.getAuthorizedGrantTypes(),
            null
        );
        clientDetails.setClientSecret(client.getSecret());
        return clientDetails;
    }
}
